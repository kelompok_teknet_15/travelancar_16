﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="TraveLancar.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Login</title>
    <meta charset ="utf-8" />
    <link href="Content/bootstrap.min.css" rel="stylesheet" />
    <link href="Content/Site.css" rel="stylesheet" />
    <script src="Content/js/jquery.js"></script>
    <script src="Content/js/bootstrap.js"></script>
    <script src="Content/js/bootstrap.min.js"></script>
</head>
<body class="body1"> 
    <div class="container">
        <p style="padding-bottom:150px;"> </p> 
        <div class="form">
            <center><img src="Content/images/logo.PNG" style="margin-bottom : 20px;padding-top:10px; width:350px;"></center>
            <form runat="server" method="POST" action="#">
                <asp:Label ID="LblWarning" CssClass="text-danger text-bold" runat="server" Text=""></asp:Label>
                <asp:TextBox ID="TxtUser" CssClass="form-control" placeholder="Username" runat="server"></asp:TextBox> <br />               
                <asp:TextBox ID="TxtPassword" CssClass="form-control"  TextMode="Password" placeholder="Password" runat="server"></asp:TextBox>   <br />           
                <asp:Button ID="BtnLogin" OnClick ="BtnLogin_Click"  CssClass ="btn btn-warning btn-block at-4 font-weigth-blod text-white" runat="server" Text="Login" />                
                <a href="~/Home/Register.cshtml"><p>Sign Up</p></a>
            </form>
        </div>
    </div>
</body>
</html>
