﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Dashboard.aspx.cs" Inherits="TraveLancar.Dashboard" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Dashboard</title>
    <meta charset ="utf-8" />
    <link href="Content/bootstrap.css" rel="stylesheet" />
    <link href="Content/Site.css" rel="stylesheet" />
    <script src="Content/js/bootstrap.js"></script>
    <script src="Content/js/jquery.js"></script>
</head>
<body>
    <!-- membuat menu navigasi -->
    <nav class="navbar navbar-default"  style="background-color : black">
        <div class="container-fluid" >
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a href="#"><img src =" / style="width : 200px ; height : 80px;"> </a>               
            </div> 
        </div>
    </nav>

    <div class="container-fluid">
        <!-- membuat jumbotron -->
        <div class="jumbotron">
            <center>
                <h2>Welcome To TraveLancar.com</h2>
                <p style="color:white;">Website Pemesana tiket Pesawat dan Pemesanan Hotel. Lengkapi Perjalanan Anda dengan TraveLancar</p><br />
            </center>
        </div>
        <!-- akhir jumbotron -->

        <div class="col-md-6">
            <div class="thumbnail" style="height: 230px;">
                <div class="caption">
                    <h3 style="text-align:center">Pesan Tiket Pesawat</h3>
                    <p style="text-align:center">
                        Terbang dengan Maskapai Terbaik di travelancar :
                            <li style="text-align:center">Puyuh Air</li>
                            <li style="text-align:center">Itik Air</li>
                            <li style="text-align:center">Pinguin Air</li>
                    </p>   
                    <p style="text-align:center"><a href="#" style="width: 300px;" class="btn btn-primary" role="button">Pesan</a></p>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="thumbnail"  style="height: 230px;">
                <div class="caption">
                    <h3 style="text-align:center">Booking Kamar Hotel</h3>
                    <p style="text-align:center">
                        Booking Kamar di Hotel Terbaik :                        
                            <li style="text-align:center">Hotel Kasur Empuk</li>
                            <li style="text-align:center">Hotel Mawar Melati</li>
                    </p>                    
                    <p style="text-align:center ;"><a href="#" style="width: 300px;" class="btn btn-primary" role="button">Pesan</a></p>
                </div>
            </div>
        </div>
    </div>

    <nav class="navbar navbar-default" style="bottom: 0;margin: 0">
        <div class="container">
            <center>
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="#">Develop by Kelompok 15</a></li>
                </ul>
            </center>
        </div>
    </nav>

</body>
</html>
