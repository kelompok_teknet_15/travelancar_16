CREATE DATABASE Travelancar

CREATE TABLE users
(
id_user INT NOT NULL PRIMARY KEY,
First_Name VARCHAR(120) NOT NULL,
Last_Name VARCHAR (20) NOT NULL,
username VARCHAR (20) NOT NULL,
PASSWORD VARCHAR (20) NOT NULL
);

INSERT INTO users VALUES ('Sopian','Manurung','if318024','if318024');

CREATE TABLE kamar
(
id_kamar INT NOT NULL PRIMARY KEY,
lokasi VARCHAR(250) NOT NULL,
check_in DATE NOT NULL,
durations VARCHAR (20),
jlh_pemesan INT NOT NULL,
Nomor_kamar INT NOT NULL
)

CREATE TABLE Hotel
(
id_hotel INT NOT NULL PRIMARY KEY,
nama_hotel VARCHAR(50) NOT NULL
)

CREATE TABLE layanan_hotel
(
id INT NOT NULL PRIMARY KEY,
id_user INT NOT NULL,
id_hotel INT NOT NULL,
id_kamar INT NOT NULL,
harga DECIMAL NOT NULL,
FOREIGN KEY(id_hotel) REFERENCES hotel(id_hotel),
FOREIGN KEY(id_kamar) REFERENCES kamar(id_kamar),
FOREIGN KEY(id_user) REFERENCES users(id_user)
)

CREATE TABLE kota(
id_kota INT PRIMARY KEY,
nama_kota CHAR (50)
)

CREATE TABLE maskapai
(
id_maskapai INT NOT NULL PRIMARY KEY,
nama_maskapai VARCHAR(50) NOT NULL
)


CREATE TABLE tiket
(
id INT NOT NULL PRIMARY KEY,
id_user INT NOT NULL,
id_maskapai INT NOT NULL,
asal INT NOT NULL,
tujuan INT NOT NULL,
harga DECIMAL NOT NULL,
FOREIGN KEY(id_maskapai) REFERENCES maskapai(id_maskapai),
FOREIGN KEY(id_user) REFERENCES users(id_user),
FOREIGN KEY(asal) REFERENCES kota(id_kota),
FOREIGN KEY(tujuan) REFERENCES kota(id_kota)
)


